##
## Makefile for  in /home/grondi_l//Projet/C++/Plazza/graph
## 
## Made by laurent grondin
## Login   <grondi_l@epitech.net>
## 
## Started on  Sun Apr 21 20:50:08 2013 laurent grondin
## Last update Wed Nov 13 00:39:55 2013 laurent grondin
##

NAME	=	gomoku

CXX	=	g++

CXXFLAGS +=	-W -Wall -Werror -Wextra -I./include $(SFMLFLAGS) -lGLEW -lGLU -lGL

SFMLFLAGS =	-I ./SFML/include -L ./SFML/lib/ -Wl,--rpath=./SFML/lib/ -lsfml-system -lsfml-window -lsfml-graphics -lsfml-audio

SRC	=	main.cpp		\
		Display.cpp		\
		Exception.cpp		\
		Core.cpp		\
		Image.cpp		\
		Case.cpp		\
		Map.cpp

OBJ	=	$(SRC:.cpp=.o)

all	:	$(NAME)

$(NAME)	:	$(OBJ)
		@$(CXX) $(CXXFLAGS) -o $@ $^
		@echo "Compilation [OK]"

clean	:
		@rm -f $(OBJ)
		@echo "Erase *.o [OK]"
		@echo "Clean [OK]"

fclean	:	clean
		@rm -f $(NAME)
		@rm -f *~
		@rm -f *#
		@echo "Erase $(NAME) [OK]"
		@echo "FClean [OK]"

re	:	fclean all
		@echo "Re [OK]"

.PHONY	:	all clean fclean re