//
// Display.hpp for  in /home/grondi_l//Projet/C/zappy/client/graphic
// 
// Made by laurent grondin
// Login   <grondi_l@epitech.net>
// 
// Started on  Thu Jun 20 16:28:23 2013 laurent grondin
// Last update Wed Nov 13 11:29:16 2013 laurent grondin
//

#ifndef __DISPLAY_HPP__
#define __DISPLAY_HPP__

#include <iostream>
#include <string>
#include <sstream>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Config.hpp>
#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include "Image.hpp"

#define HAUTEUR 64
#define LARGEUR 128
#define NB_TILE_Y 128
#define NB_TILE_X 64

class Core;

struct	Point
{
  float	x, y;
};

class	Display
{
  sf::RenderWindow	*window_;
  int			width_;
  int			height_;
  Image			image_;
  sf::View		View;
  Point			pos_map;
  Point			middle_map;
  Point			pt_text;
  Point			pt_pos;
  int			timer;
  int			isMenu;
  bool			isGame;
  sf::Vector2i		mouse;
  int			tour;

public:
  Display(int, int);
  ~Display();
  void		draw(Core &);
  void		display();
  void		clear();
  void		EventManager( Core &);
  sf::RenderWindow *getWindow() const;
  void		drawBackground(Core &);
  void		drawMenu();
  void		drawGame( Core &);
  void		drawPointMouse();
  void		drawPointCase( Core &);
  void		drawOther(Core &);
  int		tourPlayer();
  int		convertVectorDiv(int n) const;
  int		convertVectorMul(int n) const;
  bool		checkCase(Core &);
};

#endif
