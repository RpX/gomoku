//
// main.cpp for  in /home/grondi_l//Projet/C/zappy/client/graphic
// 
// Made by laurent grondin
// Login   <grondi_l@epitech.net>
// 
// Started on  Thu Jun 20 15:29:36 2013 laurent grondin
// Last update Fri Nov  8 17:33:03 2013 laurent grondin
//

#include "Core.hpp"

int	main()
{
  try
    {
      Core	*core = new Core();
      
      core->run();
    }
  catch (std::exception const & e)
    {  
      std::cout << e.what() << std::endl;
    }
  return (0);
}
