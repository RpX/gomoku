//
// Map.cpp for  in /home/petret_r//Project/unix/zappy/zappy-land/client/graphic
// 
// Made by raphael petret
// Login   <petret_r@epitech.net>
// 
// Started on  Wed Jul  3 11:51:32 2013 raphael petret
// Last update Wed Nov 13 00:38:25 2013 laurent grondin
//

#include "Map.hpp"
#include "Display.hpp"

Map::Map(int x, int y) : _x(x), _y(y)
{
}

Map::~Map()
{
}

void	Map::draw(Core &core)
{
  (void)core;
}

int	Map::convertPos(int x, int y) const
{
  return ((y * _y) + x);
}

int	Map::getSizeMap() const
{
  return (_x * _y);
}

void	Map::addCase(Case *case_)
{
  _case.push_back(case_);
}

Case    *Map::getCase(int x) const
{
  return (_case[x]);
}

int     Map::getSizeCase() const
{
  return (_case.size());
}

int	Map::getPos(char c) const
{
  if (c == 'x')
    return (_x);
  else
    return (_y);
}
