//
// Case.cpp for  in /home/petret_r//Project/unix/zappy/zappy-land/client/graphic
// 
// Made by raphael petret
// Login   <petret_r@epitech.net>
// 
// Started on  Tue Jul  9 18:24:53 2013 raphael petret
// Last update Wed Nov 13 01:14:03 2013 laurent grondin
//

#include "Case.hpp"

Case::Case(sf::Vector2i pos, int player) : pos_(pos), player_(player), isFull(false)
{
}

Case::~Case()
{
}

int		Case::getPlayer() const
{
  return (player_);
}

sf::Vector2i	Case::getPos() const
{
  return (pos_);
}

int		Case::getCoord(char c) const
{
  if (c == 'x')
    return (pos_.x / 30);
  else
    return (pos_.y / 30);
}

