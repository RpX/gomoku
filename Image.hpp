//
// Image.hpp for  in /home/grondi_l//Projet/C/zappy/client/graphic
// 
// Made by laurent grondin
// Login   <grondi_l@epitech.net>
// 
// Started on  Mon Jul  8 17:53:47 2013 laurent grondin
// Last update Wed Nov 13 11:21:12 2013 laurent grondin
//

#ifndef __IMAGE_HPP__
#define	__IMAGE_HPP__

#include <SFML/Graphics.hpp>

class	Image
{
  std::vector<sf::Image>	_image;
  sf::Texture			_tmp;
public:
  Image();
  ~Image();
  void	init();
  sf::Texture const & getImage(int);
public:
  enum
    {
      Menu,
      Menu_Player,
      Menu_IA,
      Background,
      Pt_white,
      Pt_black,
      Case_White,
      Case_Black,
      Player1,
      Player2,
    };
};

#endif
