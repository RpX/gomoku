//
// Display.cpp for  in /home/grondi_l//Projet/C/zappy/client/graphic
// 
// Made by laurent grondin
// Login   <grondi_l@epitech.net>
// 
// Started on  Thu Jun 20 16:28:21 2013 laurent grondin
// Last update Wed Nov 13 14:08:15 2013 laurent grondin
//

#include "Display.hpp"
#include "Exception.hpp"
#include "Core.hpp"
#include "Case.hpp"

Display::Display(int width, int height) : width_(width), height_(height), timer(0), isMenu(Image::Menu), isGame(false), tour(0)
{
  window_ = new sf::RenderWindow(sf::VideoMode(800, 600), "GOMOKU", sf::Style::Titlebar | sf::Style::Close);
  clear();
  window_->setFramerateLimit(60);
}

Display::~Display()
{
  delete window_;
}

sf::RenderWindow	*Display::getWindow() const
{
  return (window_);
}

void	Display::clear()
{
  window_->clear(sf::Color(185, 211, 238));
}

void	Display::EventManager(Core &core)
{
  sf::Event Event;

  while (window_->pollEvent(Event))
    {
      if (Event.type == sf::Event::Closed)
	{
      	  window_->close();
      	  throw Exception("Bye", "Bye =)\n");
	}
      if ((Event.type == sf::Event::KeyPressed && Event.key.code == sf::Keyboard::Escape))
	{
      	  window_->close();
      	  throw Exception("Bye", "Bye =)\n");
	}
      if (isGame == false)
	{
	  if (Event.type == sf::Event::KeyPressed && Event.key.code == sf::Keyboard::Down)
	    isMenu = Image::Menu_IA;
	  if (Event.type == sf::Event::KeyPressed && Event.key.code == sf::Keyboard::Up)
	    isMenu = Image::Menu_Player;
	  if (Event.type == sf::Event::KeyPressed && Event.key.code == sf::Keyboard::Return)
	    {
	      isGame = true;
	      core.resetTime();
	    }
	}
      if (isGame == true)
	{
	  mouse = sf::Mouse::getPosition(*window_);
	  if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x < convertVectorMul(20) && checkCase(core) != true)
	    {
	      core.getMap()->addCase(new Case(mouse, tourPlayer() + 6));
	      tour++;
	    }
	}
    }
}

int	Display::convertVectorDiv(int n) const
{
  return (n / 30);
}

int	Display::convertVectorMul(int n) const
{
  return (n * 30);
}

int	Display::tourPlayer()
{
  return (tour % 2);
}

bool	Display::checkCase(Core &core)
{
  if (core.getMap()->getSizeCase() == 0)
    return (false);
  for (int i = 0; i < core.getMap()->getSizeCase(); i++)
    {
      if (core.getMap()->getCase(i)->getCoord('x') == convertVectorDiv(mouse.x) && core.getMap()->getCase(i)->getCoord('y') == convertVectorDiv(mouse.y))
	return (true);
    }
  return (false);
}

void	Display::drawBackground(Core &core)
{
  int x(0), y(0);
  sf::Sprite	sprite_;
  int             tx, ty;
  
  tx = image_.getImage(Image::Background).getSize().x;
  ty = image_.getImage(Image::Background).getSize().y;
  sprite_.setTexture(image_.getImage(Image::Background));
  window_->draw(sprite_);
  
  while (y < core.getMap()->getPos('y'))
    {
      x = 0;
      while (x < core.getMap()->getPos('x'))
  	{
  	  sprite_.setPosition(x * tx, y * ty);
  	  window_->draw(sprite_);
  	  x++;
  	}
      y++;
    }
}

void	Display::drawPointMouse()
{
  sf::Sprite	sprite_;

  if (tourPlayer() == 0)
    sprite_.setTexture(image_.getImage(Image::Pt_white));
  else
    sprite_.setTexture(image_.getImage(Image::Pt_black));
  sprite_.setPosition(mouse.x - 18, mouse.y - 18);
  window_->draw(sprite_);
}

void	Display::drawPointCase(Core &core)
{
  sf::Sprite	sprite_;
  int	x, y;

  for (int i = 0; i < core.getMap()->getSizeCase(); i++)
    {
      sprite_.setTexture(image_.getImage(core.getMap()->getCase(i)->getPlayer()));
      x = convertVectorMul(convertVectorDiv((core.getMap()->getCase(i)->getPos().x)));
      y = convertVectorMul(convertVectorDiv((core.getMap()->getCase(i)->getPos().y)));
      sprite_.setPosition(x, y);
      window_->draw(sprite_);
    }
}

void	Display::drawOther(Core &core)
{
  sf::Sprite	sprite_;
  sf::Font	font;
  sf::Text	text;
  sf::Text	text2;

  if (tourPlayer() == 0)
    sprite_.setTexture(image_.getImage(Image::Player1));
  else
    sprite_.setTexture(image_.getImage(Image::Player2));
  sprite_.setPosition(640, 200);

  font.loadFromFile("images/Gomoku/digital-7.ttf");
  text.setFont(font);
  text.setString(core.convertInttoStr(core.getTime().asSeconds()));
  text.setCharacterSize(50);
  text.setPosition(740, 100);
  text.setColor(sf::Color(207, 114, 58));
  text2.setFont(font);
  text2.setString("TIME :");
  text2.setCharacterSize(50);
  text2.setPosition(630, 100);
  text2.setColor(sf::Color(207, 114, 58));
  window_->draw(sprite_);
  window_->draw(text);
  window_->draw(text2);
}

void	Display::drawGame(Core &core)
{
  drawBackground(core);
  drawPointCase(core);
  drawPointMouse();
  drawOther(core);
}

void	Display::drawMenu()
{
  sf::Sprite	sprite_;

  sprite_.setTexture(image_.getImage(isMenu));
  window_->draw(sprite_);
}

void	Display::draw(Core &core)
{
  if (isGame == false)
    drawMenu();
  else
    drawGame(core);
}

void	Display::display()
{
  window_->display();
}
