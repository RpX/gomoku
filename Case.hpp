//
// Case.hpp for  in /home/petret_r//Project/unix/zappy/zappy-land/client/graphic
// 
// Made by raphael petret
// Login   <petret_r@epitech.net>
// 
// Started on  Tue Jul  9 18:22:29 2013 raphael petret
// Last update Wed Nov 13 11:27:36 2013 laurent grondin
//

#ifndef __CASE_HPP__
#define __CASE_HPP__

#include <vector>
#include <iostream>
#include "Display.hpp"

class Case
{
  sf::Vector2i	pos_;
  int		player_;
  bool		isFull;

public:
  Case(sf::Vector2i, int);
  ~Case();
  sf::Vector2i	getPos() const;
  int		getPlayer() const;
  int		getCoord(char) const;
};

#endif
