//
// Core.cpp for  in /home/grondi_l//Projet/C/zappy/client/graphic
// 
// Made by laurent grondin
// Login   <grondi_l@epitech.net>
// 
// Started on  Wed Jul  3 11:36:53 2013 laurent grondin
// Last update Wed Nov 13 11:28:32 2013 laurent grondin
//

#include "Core.hpp"

Core::Core() : _stop(false), _x(20), _y(20)
{
  _display = new Display(WIDTH, HEIGHT);
  _map = new Map(_x, _y);
}

Core::~Core()
{
}

void		Core::run()
{
  while (!_stop)
    {
      _time = _clock.getElapsedTime();
      _display->EventManager(*this);
      _display->clear();
      _display->draw(*this);
      _display->display();
    }
}

std::string	Core::convertInttoStr(int t)
{
  std::ostringstream oss;

  oss << t;
  return (oss.str());
}

sf::Time	Core::getTime() const
{
  return (_time);
}

int		Core::getSizeMap(char c) const
{
  if (c == 'x')
    return (_x);
  else
    return (_y);
}

Map		*Core::getMap() const
{
  return (_map);
}

void		Core::resetTime()
{
  _clock.restart();
}
