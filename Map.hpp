//
// Map.hpp for  in /home/petret_r//Project/unix/zappy/zappy-land/client/graphic
// 
// Made by raphael petret
// Login   <petret_r@epitech.net>
// 
// Started on  Wed Jul  3 11:44:49 2013 raphael petret
// Last update Wed Nov 13 00:37:57 2013 laurent grondin
//

#ifndef __MAP_HPP__
#define __MAP_HPP__

#include <string>
#include <vector>
#include "Case.hpp"

class Core;

class Map
{
public:
  Map(int, int);
  ~Map();

  void	draw(Core &);
  int	getSizeMap() const;
  int	convertPos(int, int) const;
  Case *getCase(int) const;
  void	addCase(Case *);
  int	getSizeCase() const;
  int	getPos(char) const;
private:
  int	_x;
  int	_y;
  std::vector<Case *>		_case;
};

#endif
