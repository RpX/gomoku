// Core.hpp for  in /home/grondi_l//Projet/C/zappy/client/graphic
// 
// Made by laurent grondin
// Login   <grondi_l@epitech.net>
// 
// Started on  Wed Jul  3 11:35:43 2013 laurent grondin
// Last update Wed Nov 13 11:28:03 2013 laurent grondin
//

#ifndef __CORE_HPP__
#define	__CORE_HPP__

#include <iostream>
#include <string>
#include <sstream>
#include "Display.hpp"
#include "Image.hpp"
#include "Map.hpp"

#define HEIGHT  800
#define WIDTH   600

class	Core
{
  Display			*_display;
  bool				_stop;
  Map				*_map;
  int				_x;
  int				_y;
  std::vector<std::string>	_team;
  sf::Time			_time;
  sf::Clock			_clock;
public:
  Core();
  ~Core();

  void		run();
  int		getSizeMap(char) const;
  void		resizeMap(int, int);
  int		convert(const std::string &);
  void		resetTime();
  sf::Time	getTime(void) const;
  std::string	convertInttoStr(int t);
  Map		*getMap() const;
};

#endif
