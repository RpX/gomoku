//
// Exception.hpp for  in /home/petret_r//Project/c++/nibbler/nibbler-2016-grondi_l
// 
// Made by raphael petret
// Login   <petret_r@epitech.net>
// 
// Started on  Sun Mar 24 19:59:37 2013 raphael petret
// Last update Sun Mar 24 19:59:38 2013 raphael petret
//

#ifndef __EXCEPTION_HPP__
#define __EXCEPTION_HPP__

#include <string>
#include <exception>

class Exception : public std::exception
{
  std::string   str;
public:
  Exception(std::string const&, std::string const&) throw();
  virtual ~Exception() throw();

  virtual const char* what() const throw();

};

#endif
