//
// Image.cpp for  in /home/grondi_l//Projet/C/zappy/client/graphic
// 
// Made by laurent grondin
// Login   <grondi_l@epitech.net>
// 
// Started on  Mon Jul  8 17:55:40 2013 laurent grondin
// Last update Wed Nov 13 11:21:21 2013 laurent grondin
//

#include "Image.hpp"

Image::Image()
{
  init();
}

Image::~Image()
{
}

sf::Texture const &Image::getImage(int i)
{
  _tmp.loadFromImage(_image[i]);
  _tmp.setSmooth(true);
  return (_tmp);
}

void	Image::init()
{
  _image.resize(11);
  _image[Image::Menu].loadFromFile("./images/Gomoku/Menu2.bmp");
  _image[Image::Menu_Player].loadFromFile("./images/Gomoku/Menu3.bmp");
  _image[Image::Menu_IA].loadFromFile("./images/Gomoku/Menu4.bmp");
  _image[Image::Background].loadFromFile("./images/Gomoku/Case2.bmp");
  _image[Image::Pt_white].loadFromFile("./images/Gomoku/White2.bmp");
  _image[Image::Pt_black].loadFromFile("./images/Gomoku/Black2.bmp");
  _image[Image::Case_Black].loadFromFile("./images/Gomoku/Case_Black.bmp");
  _image[Image::Case_White].loadFromFile("./images/Gomoku/Case_White.bmp");
  _image[Image::Player1].loadFromFile("./images/Gomoku/Player1.png");
  _image[Image::Player2].loadFromFile("./images/Gomoku/Player2.png");
}
