//
// Exception.cpp for  in /home/petret_r//Project/c++/nibbler/nibbler-2016-grondi_l
// 
// Made by raphael petret
// Login   <petret_r@epitech.net>
// 
// Started on  Sun Mar 24 19:59:29 2013 raphael petret
// Last update Sun Mar 24 19:59:30 2013 raphael petret
//

#include "Exception.hpp"

Exception::Exception(std::string const& _str, std::string const& _str2) throw()  
{
  str += _str;
  str += _str2;
}

Exception::~Exception() throw()
{
}

const char*             Exception::what() const throw()
{
  return  (this->str.c_str());
}
